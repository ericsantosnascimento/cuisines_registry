package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.exception.NotFoundException;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void test_requesting_customers_by_cuisine_returns_right_customer() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        List<Customer> customers = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
        Assert.assertThat(customers, CoreMatchers.is(CoreMatchers.notNullValue()));
        Assert.assertThat(customers.size(), CoreMatchers.is(1));
        Assert.assertThat(customers.get(0).getUuid(), CoreMatchers.is("1"));
    }

    @Test
    public void test_requesting_customers_cuisines_returns_the_right_cuisines() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        List<Cuisine> customers = cuisinesRegistry.customerCuisines(new Customer("1"));
        Assert.assertThat(customers, CoreMatchers.is(CoreMatchers.notNullValue()));
        Assert.assertThat(customers.size(), CoreMatchers.is(2));
    }

    @Test(expected = NotFoundException.class)
    public void test_registering_invalid_cuisine_throws_not_found_expectation() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("brazilian"));
    }

    @Test
    public void test_top_three_cuisines_returns_italian_cuisine_in_first() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("turkish"));

        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(3);
        Assert.assertThat(cuisines, CoreMatchers.is(CoreMatchers.notNullValue()));
        Assert.assertThat(cuisines.size(), CoreMatchers.is(3));
        Assert.assertThat(cuisines.get(0).getName(), CoreMatchers.is("italian"));
    }

    @Test
    public void test_with_size_zero_returns_empty() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("turkish"));

        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(0);
        Assert.assertThat(cuisines, CoreMatchers.is(CoreMatchers.notNullValue()));
        Assert.assertThat(cuisines.size(), CoreMatchers.is(0));
    }


}