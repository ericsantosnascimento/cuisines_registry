package de.quandoo.recruitment.registry.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Cuisine {

    private final static List<String> VALID_CUISINES = Arrays.asList("italian", "french", "german", "turkish");

    private final String name;
    private final Set<Customer> customers = new HashSet<>();

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isCuisineValid(){
        return this.name != null && VALID_CUISINES.contains(this.name);
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;

        if (!(o instanceof Cuisine)) {
            return false;
        }

        Cuisine cuisine = (Cuisine) o;

        return cuisine.name.equals(name);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public Set<Customer> getCustomers() {
        return customers;
    }
}
