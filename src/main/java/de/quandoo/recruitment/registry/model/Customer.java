package de.quandoo.recruitment.registry.model;

import java.util.HashSet;
import java.util.Set;

public class Customer {

    private final String uuid;
    private final Set<Cuisine> cuisines = new HashSet<>();

    public Customer(final String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;

        if (!(o instanceof Customer)) {
            return false;
        }

        Customer customer = (Customer) o;

        return customer.uuid.equals(uuid);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + uuid.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.uuid;
    }

    public Set<Cuisine> getCuisines() {
        return cuisines;
    }
}
