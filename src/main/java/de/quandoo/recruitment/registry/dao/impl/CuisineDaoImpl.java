package de.quandoo.recruitment.registry.dao.impl;

import de.quandoo.recruitment.registry.dao.CuisineDao;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CuisineDaoImpl implements CuisineDao {

    private List<Cuisine> cuisines = new ArrayList<>();

    @Override
    public void save(final Cuisine cuisine) {
        cuisines.add(cuisine);
    }

    @Override
    public List<Cuisine> findCuisinesByCustomer(final Customer customer) {
        return cuisines
                .stream()
                .filter(cuisine -> cuisine.getCustomers().contains(customer))
                .collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> findTopCuisines(final int size) {

        if (size <= 0) {
            return Collections.emptyList();
        }

        return cuisines
                .stream()
                .collect(Collectors.groupingBy(cuisine -> cuisine, Collectors.counting()))
                .entrySet()
                .stream()
                .sorted((c1, c2) -> c2.getValue().compareTo(c1.getValue()))
                .map(Map.Entry::getKey)
                .limit(size)
                .collect(Collectors.toList());

    }

}
