package de.quandoo.recruitment.registry.dao;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.List;

public interface CustomerDao {

    void save(final Customer customer);

    List<Customer> findCustomerByCuisine(final Cuisine cuisine);
}
