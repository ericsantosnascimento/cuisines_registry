package de.quandoo.recruitment.registry.dao.impl;

import de.quandoo.recruitment.registry.dao.CustomerDao;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CustomerDaoImpl implements CustomerDao {

    private List<Customer> customers = new ArrayList<>();

    @Override
    public void save(final Customer customer) {
        customers.add(customer);
    }

    @Override
    public List<Customer> findCustomerByCuisine(final Cuisine cuisine) {

        if (!cuisine.isCuisineValid()) {
            return Collections.emptyList();
        }

        return customers
                .stream()
                .filter(customer -> customer.getCuisines().contains(cuisine))
                .collect(Collectors.toList());
    }
}
