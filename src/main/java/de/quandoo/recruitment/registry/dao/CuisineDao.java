package de.quandoo.recruitment.registry.dao;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.List;

public interface CuisineDao {

    void save(final Cuisine cuisine);

    List<Cuisine> findCuisinesByCustomer(final Customer customer);

    List<Cuisine> findTopCuisines(final int size);
}
