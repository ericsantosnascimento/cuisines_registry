package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.dao.CuisineDao;
import de.quandoo.recruitment.registry.dao.CustomerDao;
import de.quandoo.recruitment.registry.dao.impl.CuisineDaoImpl;
import de.quandoo.recruitment.registry.dao.impl.CustomerDaoImpl;
import de.quandoo.recruitment.registry.exception.NotFoundException;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.List;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final CustomerDao customerDao;
    private final CuisineDao cuisineDao;

    public InMemoryCuisinesRegistry() {
        this.customerDao = new CustomerDaoImpl();
        this.cuisineDao = new CuisineDaoImpl();
    }

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {

        if (cuisine.isCuisineValid()) {
            customer.getCuisines().add(cuisine);
            cuisine.getCustomers().add(customer);
            customerDao.save(customer);
            cuisineDao.save(cuisine);
        } else {
            throw new NotFoundException("Unknown cuisine, please reach johny@bookthattable.de to update the code");
        }
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return customerDao.findCustomerByCuisine(cuisine);
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return cuisineDao.findCuisinesByCustomer(customer);
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return cuisineDao.findTopCuisines(n);
    }

}
